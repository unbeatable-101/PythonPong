import turtle
import random
from mainMenu import menu
from pygame import mixer
menu()
turtle.TurtleScreen._RUNNING = True #Make Turtle able to start after turtle.bye() in mainMenu.py
turtle.mode("logo")
#Set up the screen
sc = turtle.Screen()
sc.title("Pong game")
sc.bgcolor("black")
sc.setup(width=1000, height=600)
#Set up the left paddle
leftPaddle = turtle.Turtle()
leftPaddle.speed(0)
leftPaddle.shape("square")
leftPaddle.color("red")
leftPaddle.shapesize(1, 6) #120px x 20px
leftPaddle.penup()
leftPaddle.goto(-400, 0)
#set up the right paddle
rightPaddle = turtle.Turtle()
rightPaddle.speed(0)
rightPaddle.shape("square")
rightPaddle.color("red")
rightPaddle.shapesize(1, 6) #120px x 20px
rightPaddle.penup()
rightPaddle.goto(400, 0)
#set up ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("circle")
ball.color("green")
ball.penup()
ball.goto(0, 0)
ball.right(270)
from mainMenu import ballFor,speedVarFunc
ballF=ballFor()
SpeedIncrease=speedVarFunc()
#how much the paddle moves each keypress
paddleMoveDis=20
#rightPaddle.speed(0)
#leftPaddle.speed(0)
#scoring
text=turtle.Turtle()
text.ht()
text.speed(0)
text.goto(0,275)
text.color("white")
text.write("Score", font=("andale mono",20,"bold"),align="center")

text1=turtle.Turtle()
text1.ht()
text1.speed(0)
text1.goto(0,258)
text1.color("white")
scorePlayer1=0
scorePlayer2=0
scoreText='Player 1: '+str(scorePlayer1)+'        Player 2: '+str(scorePlayer2)
text1.write(scoreText,font=("andale mono",20,"bold"),align="center")

def endGame():
    mixer.music.stop()
    turtle.bye()
    exit()

def winner(player):
    mixer.music.stop()
    turtle.clearscreen()
    sc.bgcolor("black")
    winnerText=turtle.Turtle()
    winnerText.goto(0,0)
    winnerTextText='Player '+str(player)+' Wins!'
    winnerText.color('white')
    winnerText.write(winnerTextText,font=("andale mono",20,"bold"),align="center")

def leftUp():
    leftPaddle.sety(leftPaddle.ycor()+paddleMoveDis)
    if leftPaddle.ycor() >= 250:
        leftPaddle.sety(leftPaddle.ycor()-30)

def leftDown():
    leftPaddle.sety(leftPaddle.ycor()-paddleMoveDis)
    if leftPaddle.ycor() <= -250:
        leftPaddle.sety(leftPaddle.ycor()+30)

def rightUp():
    rightPaddle.sety(rightPaddle.ycor()+paddleMoveDis)
    if rightPaddle.ycor() >= 250:
        rightPaddle.sety(rightPaddle.ycor()-30)

def rightDown():
    rightPaddle.sety(rightPaddle.ycor()-paddleMoveDis)
    if rightPaddle.ycor() <= -250:
        rightPaddle.sety(rightPaddle.ycor()+30)

turtle.listen()
turtle.onkey(leftUp,"w")
turtle.onkey(leftDown,"s")
turtle.onkey(rightUp,"o")
turtle.onkey(rightDown,"k")
turtle.onkey(endGame,'f')

inited=mixer.get_init()
if inited is None:
    mixer.init()
mixer.music.stop()
mixer.music.load("audio/background.mp3")
mixer.music.play(-1)
mixer.music.set_volume(0.5)

ballHit=mixer.Sound('audio/hit.wav')
ballHitEnd=mixer.Sound('audio/ballHitEnd.mp3')
#ballHitSide=mixer.Sound('audio/hit.wav')
#Winner=mixer.Sound('audio/hit.wav')

#scores that will increase the speed of the ball
speedScores=[5,10,20,30,40]

while True:
    sc.update() #slows animation
    ball.forward(ballF)
    if ball.xcor()>= 500:
        ballHitEnd.play()
        scorePlayer1+=1
        text1.clear()
        scoreText='Player 1: '+str(scorePlayer1)+'        Player 2: '+str(scorePlayer2)
        text1.write(scoreText,font=("andale mono",20,"bold"),align="center")
        if scorePlayer1 in speedScores:
            ballF +=SpeedIncrease
        if scorePlayer1==50:
            winner(1)
        ball.goto(0,0)
        ball.setheading(270)
    if ball.xcor()<=-500:
        ballHitEnd.play()
        scorePlayer2+=1
        text1.clear()
        scoreText='Player 1: '+str(scorePlayer1)+'        Player 2: '+str(scorePlayer2)
        text1.write(scoreText,font=("andale mono",20,"bold"),align="center")
        if scorePlayer2 in speedScores:
            ballF +=SpeedIncrease
        if scorePlayer2==50:
            winner(2)
        ball.goto(0,0)
        ball.setheading(90)
    if ball.ycor()>300:
        ballHit.play()
        ball.sety(290)
        if ball.heading() in range(0,179):
            ball.setheading(random.randint(105,165))
        else:
            ball.setheading(random.randint(195,255))
    if ball.ycor()<-300:
        ballHit.play()
        ball.sety(-290)
        if ball.heading() in range(0,179):
            ball.setheading(random.randint(15,75))
        else:
            ball.setheading(random.randint(285,345)) #225,285
#Collision detection
    if ball.xcor()>380 and ball.xcor()<400 and (ball.ycor()<rightPaddle.ycor()+65 and ball.ycor()>rightPaddle.ycor()-65):
        ball.left(random.randint(150,200))
        ball.setx(ball.xcor()-5)
        ballHit.play()

    if ball.xcor()<-380 and ball.xcor()>-400 and (ball.ycor()<leftPaddle.ycor()+65 and ball.ycor()>leftPaddle.ycor()-65):
        ball.right(random.randint(150,200))
        ball.setx(ball.xcor()+5)
        ballHit.play()
