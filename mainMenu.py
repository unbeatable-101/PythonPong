import turtle
from pygame import mixer
from tkinter.messagebox import showinfo
screenNum=0
ballF=8
speedVar=2

#setup screen
sc = turtle.Screen()
sc.title("Pong·game")
sc.bgcolor("white")
sc.setup(width=800, height=600)

def menu():

    mixer.init()
    mixer.music.load("audio/menu.mp3")
    mixer.music.play(-1)
    #setup buttons
    leftButton=turtle.Turtle()
    leftButton.up()
    leftButton.speed(0)
    leftButton.shape("square")
    leftButton.fillcolor("#e0ff00")
    leftButton.shapesize(3, 6)
    leftButton.goto(-200,-100)
    def bill(c,r):
        global screenNum
        if screenNum == 0:
            showinfo('Instructions','Use "w" and "s" to move player one up and down, use "o" and "k" to move player two up and down; only press one key at a time to move. When someone gets five points or the score is a multiple of ten, the speed of the ball will increase. The first person to 50 points wins!')
        elif screenNum ==1:
            mixer.music.stop()
            turtle.resetscreen() #This is needeed to avoid a memory error
            turtle.bye()
        elif screenNum==2:
            global ballF
            ballF=sc.numinput("Settings", "Pick the movement speed of the ball, deafult is 8", 8, minval=2, maxval=20)
    leftButton.onclick(bill)
    leftButton.write("Instructions",align='center',font=('Arial', 15, 'bold'))



    rightButton=turtle.Turtle()
    rightButton.up()
    rightButton.speed(0)
    rightButton.shape("square")
    rightButton.fillcolor("#e0ff00")
    rightButton.shapesize(3, 6)
    rightButton.goto(200,-100)
    def speedIncrease(c,r):
        global speedVar
        speedVar=sc.numinput("Settings", "Pick the movement speed increase of the ball, deafult is 2", 2, minval=1, maxval=10)
    def steve(c,r):
        global screenNum
        if screenNum==2:
            middleButton.ht()
            middleButton.clear()
        screenNum=1
        rightButton.clear()
        leftButton.clear()
        rightButton.shape("square")
        rightButton.fillcolor("#e0ff00")
        def settings(c,t):
            global screenNum
            screenNum=2
            rightButton.clear()
            leftButton.clear()
            rightButton.onclick(steve)
            rightButton.write("Back",align='center',font=('Arial', 15, 'bold'))
            leftButton.write("Movement speed",align='center',font=('Arial', 15, 'bold'))
            global middleButton
            middleButton=turtle.Turtle()
            middleButton.ht()
            middleButton.up()
            middleButton.speed(0)
            middleButton.shape("square")
            middleButton.fillcolor("#e0ff00")
            middleButton.shapesize(3, 6)
            middleButton.goto(-0,-100)
            middleButton.st()
            middleButton.onclick(speedIncrease)
            middleButton.write("Speed increase",align='center',font=('Arial', 15, 'bold'))
        rightButton.onclick(settings)
        rightButton.write("Settings",align='center',font=('Arial', 15, 'bold'))

        leftButton.shape("square")
        leftButton.fillcolor("#e0ff00")
#        def play(c,t):
#            turtle.done()
        leftButton.onclick(bill)
        leftButton.write("2 Players",align='center',font=('Arial', 15, 'bold'))
    rightButton.onclick(steve)
    rightButton.write("Play",align='center',font=('Arial', 15, 'bold'))
    turtle.mainloop()
def ballFor():
    return ballF
def speedVarFunc():
    return speedVar
